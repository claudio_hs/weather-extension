var createElement = function (tag, parent, innerText, attributes) {
    var element = document.createElement(tag);
    parent.appendChild(element);
    if (innerText)
        element.innerText = innerText;
    if (attributes) {
        for (attr in attributes)
            element.setAttribute(attr, attributes[attr]);
    }
    return element;
}

var insertDailyForecasts = function (forecasts) {
    var forecastsContainer, individualContainer, classContainer, forecast, lang, text, tempRange;
    forecastsContainer = document.querySelector("#forecasts");
    lang = chrome.i18n.getUILanguage();
    for (var i = 0; i < forecasts.length; i++) {
        forecast = forecasts[i];
        classContainer = i==0? "today":"futureDay";
        individualContainer = createElement("div", forecastsContainer, null, {"class":classContainer});
        
        text = (new Date(forecast.EpochDate * 1000)).toLocaleString(lang, {weekday: i==0? "long":"short"});
        createElement("h3", individualContainer, text);
        
        createElement("img", individualContainer, null, {
            "src":"../images/accuweather-icons/" + forecast.Day.Icon + ".svg",
            "class":"day"
        });
        createElement("img", individualContainer, null, {
            "src":"../images/accuweather-icons/" + forecast.Night.Icon + ".svg",
            "class":"night"
        });
        
        tempRange = createElement("div", individualContainer, null, {"class":"tempRange"});
        text = forecast.Temperature.Maximum.Value + "\xB0" + forecast.Temperature.Maximum.Unit;
        createElement("span", tempRange, text, {"class":"max"});
        text = forecast.Temperature.Minimum.Value + "\xB0" + forecast.Temperature.Minimum.Unit;
        createElement("span", tempRange, text, {"class":"min"});
        
        createElement("p", individualContainer, forecast.Day.IconPhrase);
        
        createElement("a", individualContainer, null, {"href":forecast.Link, "target":"_blank"});
    }
}

var celsiusToFahrenheit = function (c) {
    return ((c * 1.8) + 32).toFixed(1);
}

var changeUnit = function (toCelsius) {
    var newUnit = toCelsius? "C":"F";
    chrome.storage.local.get(["forecasts", "currentConditions"], function (storage) {
        var forecast;
        var minTemperatures = document.querySelectorAll("div.tempRange span.min");
        var maxTemperatures = document.querySelectorAll("div.tempRange span.max");
        for (var i = 0; i < storage.forecasts.length; i++) {
            temperature = storage.forecasts[i].Temperature;
            if (!toCelsius) {
                temperature.Minimum.Value = celsiusToFahrenheit(temperature.Minimum.Value);
                temperature.Maximum.Value = celsiusToFahrenheit(temperature.Maximum.Value);
            }
            minTemperatures[i].innerText = temperature.Minimum.Value + "\xB0" + newUnit;;
            maxTemperatures[i].innerText = temperature.Maximum.Value + "\xB0" + newUnit;;
        }
        
        var temp = storage.currentConditions.Temperature[toCelsius? "Metric":"Imperial"].Value.toString();
        chrome.browserAction.setBadgeText({text:temp});
    });
}

var initUnitSwitch = function () {
    var unitSwitch = document.querySelector("#unit");
    unitSwitch.addEventListener("change", function (e) {
        chrome.storage.local.set({"metric":this.checked});
        if (this.checked)
            document.body.classList.add("celsius");
        else
            document.body.classList.remove("celsius");
        changeUnit(this.checked);
    });
    chrome.storage.local.get({"metric":true}, function (storage) {
        unitSwitch.checked = storage.metric;
        if (storage.metric)
            document.body.classList.add("celsius");
    });
}

document.addEventListener("DOMContentLoaded", function(event) {
    document.querySelector("p#providedBy a").innerText = chrome.i18n.getMessage("providedBy", "AccuWeather.com");
    chrome.storage.local.get({
        "location":{},
        "forecasts":[],
        "headline":{},
        "currentConditions":{},
        "metric":true
    }, function (storage) {
        document.body.classList.add(storage.currentConditions.IsDayTime? "day":"night");
        document.querySelector("#locationName").innerText = storage.location.name;
        document.querySelector("#headline").innerText = storage.headline.Text;
        insertDailyForecasts(storage.forecasts);
        initUnitSwitch();
        if (!storage.metric)
            changeUnit(false);
    });
});