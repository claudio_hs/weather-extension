var apikey = "B7KsFKuew9zNBUMYDx0scUNTirrEhAN2";
var metric = true;
var language = chrome.i18n.getUILanguage();

chrome.storage.local.get({"metric":true}, function (storage) {
    metric = storage.metric;
});

chrome.storage.onChanged.addListener(function (changes, areaName) {
    if (changes.metric)
        metric = changes.metric.newValue;
});

var loadCoords = function (callback) {
    navigator.geolocation.watchPosition(function (pos) {
        // A precision of 3 digits on the latitude and longitude is good enough for the extension purposes (see https://en.wikipedia.org/wiki/Decimal_degrees#Precision )
        // Note: The geolocation API is not 100% precise, using a low precision while storing the data will prevent that the script detect a false change in the users current location
        var latitude = pos.coords.latitude.toFixed(2);
        var longitude = pos.coords.longitude.toFixed(2);
        callback(latitude, longitude);
    });
}

var loadLocationId = function (latitude, longitude, callback) {
    var url = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=" + apikey + "&q=" + latitude + "%2C" + longitude + "&language=" + language;
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.response);
            chrome.storage.local.set({
                location: {
                    latitude: latitude,
                    longitude: longitude,
                    name: response.LocalizedName,
                    id: response.Key
                }
            });
            callback(response.Key);
        }
    };
    request.open("GET", url, true);
    request.send();
}

var load5DaysForecast = function (locationId) {
    var url = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/" + locationId + "?apikey=" + apikey + "&metric=true&language=" + language;
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.response);
            chrome.storage.local.set({
                forecasts: response.DailyForecasts,
                headline: response.Headline
            });
        }
    };
    request.open("GET", url, true);
    request.send();
}

var loadCurrentConditions = function (locationId) {
    var url = "http://dataservice.accuweather.com/currentconditions/v1/" + locationId + "?apikey=" + apikey + "&language=" + language;
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.response);
            chrome.storage.local.set({
                currentConditions: response[0]
            });
            updateBrowserAction(response[0]);
        }
    };
    request.open("GET", url, true);
    request.send();
}

var load5DaysForecastAndCurrentConditions = function (locationId) {
    load5DaysForecast(locationId);
    loadCurrentConditions(locationId);
}

var updateBrowserAction = function (currentConditions) {
    var temp = currentConditions.Temperature[metric? "Metric":"Imperial"].Value.toString();
    var iconPath = "../images/accuweather-icons/" + currentConditions.WeatherIcon + ".svg";
    chrome.browserAction.setBadgeText({text:temp});
    chrome.browserAction.setIcon({path:iconPath});
}

var checkSameDay = function (epochTime1, epochTime2) {
    var date1 = new Date(epochTime1);
    var date2 = new Date(epochTime2);
    return date1.toDateString() == date2.toDateString();
}

var checkStoredData = function () {
    // Checks if the stored data is not outdated or nonexistent
    chrome.storage.local.get(["location", "forecasts"], function (storage) {
        loadCoords(function (latitude, longitude) {
            if (!storage.location || // If there is no location information stored yet or...
                storage.location.latitude != latitude || storage.location.longitude != longitude) // ...if the user is now in a different location...
                loadLocationId(latitude, longitude, load5DaysForecastAndCurrentConditions); // then load the location id and the forecasts
            else { // we already have the correct location information
                if (!storage.forecasts || // if there is no forecast information stored or...
                    !checkSameDay(Date.now, storage.forecasts[0].EpochDate * 1000)) // ... if the forecast stored is from a past date
                    load5DaysForecastAndCurrentConditions(storage.location.id);
            }
        });
    });
}

var setAlarms = function () { // Schedules a daily alarm to update the forecasts
    var tomorrow = new Date((new Date().getTime() + 24 * 60 * 60 * 1000));
    tomorrow.setHours(0, 0, 0, 0);
    chrome.alarms.create("dailyUpdate", {
        when: tomorrow.getTime(),
        periodInMinutes: 24 * 60 // repeat daily (period of 24 * 60 * 60 minutes)
    });
    
    chrome.alarms.create("hourlyUpdate", {
        when: Date.now(),
        periodInMinutes: 60 // repeat hourly (period of 60 minutes)
    });
}

chrome.alarms.onAlarm.addListener(function (alarm) {
    switch (alarm.name) {
        case "dailyUpdate":
            checkStoredData();
            break;
        case "hourlyUpdate":
            chrome.storage.local.get("location", function (storage) {
                if (storage.location)
                    loadCurrentConditions(storage.location.id);
            });
            break;
    }
});

checkStoredData();
setAlarms();